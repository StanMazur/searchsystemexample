﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using SearchSystemServer;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;


namespace UTestSearchSystem
{
   public class Server
    {
        public readonly TestServer TestServer;
        public readonly HttpClient ApiClient;


        public Server()
        {
            var cloudBuilder = new WebHostBuilder()
                .UseStartup<Startup>();

            TestServer = new TestServer(cloudBuilder);

            ApiClient = TestServer.CreateClient();

        }

    }
}
