using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Net.Http;
using System.Threading.Tasks;
using DAL.Models;
using LogicLayer;
using LogicLayer.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using  Moq;
using Newtonsoft.Json;
using Repositories;
using Repositories.Base;

namespace UTestSearchSystem
{
    [TestClass]
    public class ControllersTest
    {
        [TestMethod]
        public async Task LastQuery()
        {
            var server = new Server();

            var response = await server.ApiClient.GetAsync($"api/SearchSystem/GetLastQuery");
            var res = JsonConvert.DeserializeObject<BaseResponse>(await response.Content.ReadAsStringAsync());

            Assert.IsNotNull(res.data);
        }

        [TestMethod]
        public async Task NewQuery()
        {
            var server = new Server();
            var query = "Best last week photo";

            var response = await server.ApiClient.GetAsync($"api/SearchSystem/GetNewQuery?={query}");
            var res = JsonConvert.DeserializeObject<BaseResponse>(await response.Content.ReadAsStringAsync());

            Assert.IsNotNull(res.data);
        }

    }
}
