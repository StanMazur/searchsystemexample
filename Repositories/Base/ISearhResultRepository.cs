﻿using System;
using System.Collections.Generic;
using DAL.Models;

namespace Repositories.Base
{
    public interface ISearhResultRepository : IBaseRepository<search_results>
    {
        void AddResult(Guid searchReq_id, List<string> queryResult, string searchSystem);
        List<search_results> GetByRequest(Guid searchReq_id);
    }
}
