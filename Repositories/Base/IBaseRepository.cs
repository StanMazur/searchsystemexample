﻿using DAL.Models;
using System;
using System.Collections.Generic;

namespace Repositories.Base
{
    public interface IBaseRepository<EntityType> : IRepository
        where EntityType : BaseTable
    {
        List<EntityType> GetAll();

        EntityType GetByID(Guid ID);
        Guid Add(EntityType item);
        void RemoveByID(Guid ID);

        void Remove(EntityType item);
    }
}
