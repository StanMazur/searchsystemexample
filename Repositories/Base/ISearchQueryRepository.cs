﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repositories.Base
{
    public interface ISearchQueryRepository : IBaseRepository<search_queries>
    {
        search_queries GetLastQuery();
    }
}
