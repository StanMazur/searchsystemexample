﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Repositories.Base;

namespace Repositories
{
    public class BaseRepository<EntityType> : IBaseRepository<EntityType> 
        where EntityType : BaseTable
    {
        protected readonly DbContext context;

        protected BaseRepository() { }

        public BaseRepository(DbContext context)
        {
            this.context = context;
        }

        public Guid Add(EntityType item)
        {
            if (item == null) throw new Exception("Can't add null item");
            item.id = Guid.NewGuid();
            item.creation_time = item.modi_time = DateTime.Now;
            context.Add(item);
            context.SaveChanges();
            return item.id;
        }

        public List<EntityType> GetAll() => context.Set<EntityType>().ToList();

        public EntityType GetByID(Guid ID) => context.Set<EntityType>().FirstOrDefault(t => t.id == ID);
        

        public void Remove(EntityType item)
        {
            if (item != null)
            {
                context.Remove(item);
                context.SaveChanges();
            }
        }

        public void RemoveByID(Guid ID)
        {
            var item = context.Set<EntityType>().FirstOrDefault(t => t.id == ID);
            if (item != null)
            {
                context.Set<EntityType>().Remove(item);
                context.SaveChanges();
            }
        }
    }
}
