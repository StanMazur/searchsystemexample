﻿using DAL;
using System;
using System.Collections.Generic;
using System.Text;
using Repositories.Base;

namespace Repositories.RepositoryManager
{
    public interface IRepositoryManager : IRepository
    {
        T Get<T>(ApplicationContext db) where T : IRepository;
    }
}
