﻿using DAL;
using Repositories.Base;
using System;

namespace Repositories.RepositoryManager
{
    public class RepositoryManager : IRepositoryManager
    {
        public T Get<T>(ApplicationContext db)
            where T : IRepository
        {
            return (T)Activator.CreateInstance(typeof(T), db);
        }
    }
}
