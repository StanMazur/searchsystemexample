﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Repositories.Base;

namespace Repositories
{
    public class SearchQueryRepository : BaseRepository<search_queries>, ISearchQueryRepository
    {
        protected SearchQueryRepository() : base() { }
        public SearchQueryRepository(DbContext context) : base(context) { }
        public search_queries GetLastQuery()
        {
            if (context == null) throw new Exception("Database context is not created");
            return context
                .Set<search_queries>()
                .OrderByDescending(t => t.creation_time)
                .First();
        }
    }
}
