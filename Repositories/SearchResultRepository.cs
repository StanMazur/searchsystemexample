﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Repositories.Base;

namespace Repositories
{
    public class SearchResultRepository : BaseRepository<search_results>, ISearhResultRepository
    {
        protected SearchResultRepository() : base() { }
        public SearchResultRepository(DbContext context) : base(context) { }
        public void AddResult(Guid searchQuery_id, List<string> queryResults, string searchSystem)
        {
            if (context == null) throw new Exception("Database context is not created");
            var dateAdd = DateTime.Now;
            var searchres = new List<search_results>();
            if (queryResults != null && queryResults.Count > 0)
            {
                foreach (var href in queryResults)
                {
                    searchres.Add(new search_results
                        {
                            creation_time = dateAdd,
                            id = Guid.NewGuid(),
                            modi_time = dateAdd,
                            query_id = searchQuery_id,
                            search_result = href,
                            search_system = searchSystem
                        }
                    );
                }
            }
            context.Set<search_results>().AddRange(searchres);
            context.SaveChanges();
        }

        public List<search_results> GetByRequest(Guid searchQuery_id)
        {
            if (context == null) throw new Exception("Database context is not created");
            return context.Set<search_results>()
                .Where(t => t.query_id == searchQuery_id)
                .ToList();
        }
    }
}
