﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    //base model of table
    public abstract class BaseTable
    {
        public Guid id { get; set; }
        public DateTime creation_time { get; set; }
        public DateTime modi_time { get; set; }
    }
}
