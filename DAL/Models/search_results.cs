﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    //table for keep search result
    public class search_results : BaseTable
    {
        public Guid query_id{ get; set; }
        public string search_result { get; set; }
        public string search_system { get; set; }
    }
}
