﻿using CommonLibs;
using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class ApplicationContext : DbContext
    {
        //queries table
        public DbSet<search_queries> search_queries { get; set; }
        //result table
        public DbSet<search_results> search_results { get; set; }

        public ApplicationContext()
        {
            if (Database.EnsureCreated()) Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(AppConfiguration.connectionString);
        }

    }
}
