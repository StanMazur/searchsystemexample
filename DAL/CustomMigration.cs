﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class CustomMigration
    {
        public static void StartMigration()
        {
            using (var db = new ApplicationContext())
            {
                CreateMigration(db);
            }
        } 
        /// <summary>
        /// read tables names
        /// </summary>
        /// <returns>All tables names in the List of the string</returns>
        private static List<string> Tables ()
        {
            var tablesbNames = new List<string>();
            using (var db = new ApplicationContext())
            {
                using (var dr = db.Database.ExecuteSqlQuery($"SELECT * FROM INFORMATION_SCHEMA.TABLES"))
                {
                    var reader = dr.DbDataReader;
                    while (reader.Read())
                    {
                        tablesbNames.Add(reader[2].ToString()); // third column contain table name
                    }
                    return tablesbNames;
                }
            }
        }

        private static void CreateMigration(DbContext db)
        {
            var tables = typeof(CustomMigration).Assembly
                .GetTypes()
                .Where(t => t.IsClass && !t.IsAbstract && t.BaseType == typeof(BaseTable))
                .ToList();
            if (tables != null && tables.Count > 0)
            {
                tables.ForEach(t =>
                {
                    //1.проверим есть ли такая таблица в принципе
                    var isCreateTable = false;
                    using (var dr = db.Database.ExecuteSqlQuery($"SELECT* FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = '{t.Name.ToLower()}'"))
                    {
                        var reader = dr.DbDataReader;
                        if (!reader.Read())
                        {
                            isCreateTable = true;
                        }
                    }

                    var columns = t.GetProperties();

                    if (isCreateTable)
                    {
                        //надо создать таблицу
                        var d = db.Database.ExecuteSqlQuery(@"CREATE TABLE " + t.Name.ToLower() + @" (
                                                     id UNIQUEIDENTIFIER PRIMARY KEY,
                                                     " +
                                                            columns
                                                                .Where(c => c.Name.ToLower() != "id")
                                                                .Aggregate(String.Empty,
                                                                    (x, y) =>
                                                                        x +
                                                                        $"{y.Name.ToLower()} {GetDbType(y.PropertyType, y.GetCustomAttributes(true))}" +
                                                                        ",",
                                                                    (final) => final.Length > 0
                                                                        ? final.Substring(0, final.Length - 1)
                                                                        : final) +
                                                            ");");
                        d.Dispose();

                    }
                    else
                    {
                        //не надо создавать таблицу, она уже существует
                        var dbColNames = new List<string>();
                        using (var dr = db.Database.ExecuteSqlQuery(@"SELECT c.column_name as name
                                                            FROM information_schema.columns c
                                                            WHERE table_name   = '" + t.Name.ToLower() + "'"))
                        {
                            var reader = dr.DbDataReader;
                            while (reader.Read())
                            {
                                dbColNames.Add((string) (reader as IDataReader)["name"]);
                            }
                        }

                        var newCols = columns
                            .Where(c => dbColNames.FindIndex(dbname =>
                                            dbname.Equals(c.Name.ToLower(),
                                                StringComparison.CurrentCultureIgnoreCase)) < 0)
                            .ToList();

                        if (newCols != null && newCols.Count > 0)
                        {
                            //добавим столбцы, которых не хватает
                            newCols.ForEach(c =>
                            {
                                var d = db.Database.ExecuteSqlQuery(
                                    $"ALTER TABLE {t.Name.ToLower()} ADD COLUMN {c.Name.ToLower()} {GetDbType(c.PropertyType, c.GetCustomAttributes(true))};");
                                d.Dispose();
                            });
                        }

                    }
                });
            }
        }

        /// <summary>
        /// converter for data types for castom migration
        /// </summary>
        /// <param name="type"></param>
        /// <param name="attributes"></param>
        /// <returns></returns>
        private static string GetDbType(Type type, object[] attributes)
        {
            if (type == typeof(int) || type == typeof(int?) || type == typeof(long) || type == typeof(long?))
            {
                return "INTEGER";
            }
            if (type == typeof(short) || type == typeof(short?))
            {
                return "SMALLINT";
            }
            else if (type == typeof(double) || type == typeof(float) || type == typeof(double?) || type == typeof(float?))
            {
                return "FLOAT(8)";
            }
            else if (type == typeof(DateTime) || type == typeof(DateTime?))
            {
                return "DATETIME";
            }
            else if (type == typeof(Guid) || type == typeof(Guid?))
            {
                return "UNIQUEIDENTIFIER";
            }

            return "VARCHAR";
        }
    }
}
