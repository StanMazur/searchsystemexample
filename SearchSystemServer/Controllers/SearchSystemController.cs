﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogicLayer;
using LogicLayer.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace SearchSystemServer.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SearchSystemController : ControllerBase
    {
        //Post api/SearchSystem
        [HttpGet]
        [DisableCors]
        public BaseResponse GetNewQuery([FromQuery]string query)
        {
            return new SearchSystemInfra().QueryToSearchSystem(query);
        }

        // Get api/SearchSystem
        [HttpGet]
        [DisableCors]
        public BaseResponse GetLastQuery()
        {
            return new SearchSystemInfra().GetLastQuery();
        }
    }
}
