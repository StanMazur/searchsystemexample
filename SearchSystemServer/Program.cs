﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace SearchSystemServer
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var hostingConfig = new ConfigurationBuilder()
                .AddJsonFile("hosting.json", optional: false)
                .Build();

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseConfiguration(hostingConfig)
                .UseStartup<Startup>()
                .UseWebRoot(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "front"))
                .Build();

            host.Run();
        }
    }
}
