import React from 'react';

export default class DataSource {
     getLastquery(){
        const uri = '/api/SearchSystem/GetLastQuery';
        return {uri}
    }

    sendQuery(query){
        const uri = '/api/SearchSystem/GetNewQuery?='+query;
        return {uri}
    }
}