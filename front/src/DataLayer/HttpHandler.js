import React from 'react';

export default class HttpHandler {
    async GetData(uri){
        let res = await fetch(uri, {method: 'GET'})
            .then( response =>  response.json())
            .then( apiData =>{return apiData})
            .catch( error => {return error});

        return res;
    }
}