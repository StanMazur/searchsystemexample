import React, {Component} from 'react';
import './App.css';
import DataSource from "./DataLayer/DataSource";
import HttpHandler from "./DataLayer/HttpHandler";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchSystem: '',
            query:'',
            results: []
        };
        this.handlerGetLast = this.handlerGetLast.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event) {
        this.setState({query: event.target.value});
    }
    async handlerGetLast(){
        let source = new DataSource();
        let hh = new HttpHandler();
        let uri = this.state.query == '' ? source.getLastquery() : source.sendQuery(this.state.query);
        console.log(uri);
        let res = await hh.GetData(uri.uri);

        this.setState({
            searchSystem: res.searhSystem,
            query:res.query,
            results: res.data
            });
    }

    async handlerGetNewQuery(){
        let source = new DataSource();
        let hh = new HttpHandler();
        let res = await hh.GetData(source.sendQuery(this.state.query).uri);

        this.setState({
            searchSystem: res.searhSystem,
            query:res.query,
            results: res.data
        });
    }
    componentWillMount() {
        let res = this.handlerGetLast();
    };
      render() {
        return(
          <div className="body">
              <h1 className='h1'>Search Results</h1>
              <h2 className='h2'>Search System: {this.state.searchSystem}</h2>
              <h2 className='h2'>
                  Query: {this.state.query}
                  <input type="text" value={this.state.query} onChange={this.handleChange}/>
                  </h2>
              {
                  this.state.results.length > 0 ? this.state.results.map(result => {
                      return <h3 className='h3'> {result} </h3>
                  }) : null
              }
              <button onClick={this.handlerGetLast} className="button">
                  Поиск
              </button>
          </div>
      )
    }
}
