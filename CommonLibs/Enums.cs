﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonLibs
{
    public class Enums
    {
        public enum ServerStatus
        {
            OK = 0,
            TCP_UNKNOWN_ERROR = 1, //неизвестная ошибка
            API_UNKNOWN_ERROR = 100,        //неизвестная ошибка
            COMMON_HTTP_SERVICE_NOT_AVAILABLE = 200 //http сервис недоступен
        }
    }
}
