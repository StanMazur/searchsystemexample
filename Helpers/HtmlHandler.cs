﻿using System;
using System.Collections.Generic;
using CsQuery;
using CsQuery.ExtensionMethods.Internal;

namespace Helpers
{
    public class HtmlHandler
    {
        /// <summary>
        /// Method convert Html string into list of string containing search results
        /// </summary>
        /// <param name="html">html data for parsing</param>
        /// <param name="searchSystem">name of search system for correcting resulting data</param>
        /// <returns>list<string> containing search result</returns>
        public static List<string> GetHref(string html, string searchSystem)
        {
            if (html.IsNullOrEmpty()) throw new Exception("Can't parse empty or null data string");
            if (searchSystem.IsNullOrEmpty()) throw new Exception("SearchSystem name must be not null");
            var refs = new List<string>();
            try
            {
                var cq = CQ.Create(html);
                foreach (var DOMobj in cq.Find("a"))
                {
                    var referens = DOMobj.GetAttribute("href");
                    if (referens != null)
                        if (referens.Contains("http") && (referens.Contains(searchSystem) == false)) //remove unnecessary link
                        {
                            if (searchSystem.ToLower() == "google") //handle result from google
                            {
                                if (referens.Contains("/url?q="))
                                    refs.Add(referens.Replace("/url?q=", ""));
                                continue;
                            };
                            if (searchSystem.ToLower() == "bing")//handle result from bing
                            {
                                if (referens.Contains("microsoft") == false) refs.Add(referens);
                                continue;
                            }
                            refs.Add(referens);
                        }
                }
                return refs;
            }
            catch (Exception e)
            {
                throw new Exception("Error parse html data " + e);
            }
            
        }
    }
}
