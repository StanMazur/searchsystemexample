﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Helpers
{
    public class HttpHandler
    {
        private string uri;

        public HttpHandler(string uri)
        {
            this.uri = uri;
        }

        public HttpHandler() { }

        public static string GetMethod(string req, string url, string user = null, string password = null, string token = null)
        {
            var result = string.Empty;
            var uri = url + req;
            var request = (HttpWebRequest)WebRequest.Create(uri);
            if (!string.IsNullOrEmpty(user))
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                request.Credentials = new NetworkCredential(user, password);
            }

            if (token != string.Empty)
            {
                request.Headers.Add("Authorization", token);
            }

            request.Method = "GET";
            request.ContentType = "text/plain;charset=UTF-8";

            // ответ
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception($"HTTP error {response.StatusCode} at request: {response.StatusDescription}");
                }
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }
}
