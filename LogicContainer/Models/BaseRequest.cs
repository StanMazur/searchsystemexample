﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogicLayer.Models
{
    /// <summary>
    /// model of base request from client
    /// </summary>
    public class BaseRequest
    {
        public string query { get; set; }
    }
}
