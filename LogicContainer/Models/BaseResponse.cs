﻿

using System.Collections.Generic;

namespace LogicLayer.Models
{
    /// <summary>
    /// model of base response to client
    /// </summary>
    public class BaseResponse
    {
        public int status { get; set; }
        public string query { get; set; }
        public string searhSystem { get; set; }
        public object data { get; set; }
    }
}
