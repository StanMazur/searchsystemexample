﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace LogicLayer.Models
{
    public class SearchSystems
    {
        [JsonProperty("SearchSystems")]
        public List<SearchSystem> SearchSysytem { get; set; }
    }
}
