﻿
using Newtonsoft.Json;

namespace LogicLayer.Models
{
    public class SearchSystem
    {
        [JsonProperty("SystemName")]
        public string SystemName { get; set; }
        [JsonProperty("Uri")]
        public string Uri { get; set; }
    }
}
