﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using DAL.Models;
using Helpers;
using LogicLayer.Models;
using Newtonsoft.Json;
using Repositories;
using Repositories.RepositoryManager;
using CommonLibs;


namespace LogicLayer
{
    public class SearchSystemInfra
    {
        private const int offset = 0;
        private const int limit = 10;
        public IRepositoryManager repManager { get; set; }

        public SearchSystemInfra()
        {
            repManager = new RepositoryManager();
        }

        public SearchSystemInfra(IRepositoryManager manager)
        {
            repManager = manager;
        }

        /// <summary>
        /// Method for getting data from last request
        /// </summary>
        /// <returns></returns>
        public BaseResponse GetLastQuery()
        {
            using (var db = new ApplicationContext() )
            {
                var sq = repManager.Get<SearchQueryRepository>(db);
                var res = repManager.Get<SearchResultRepository>(db).GetByRequest(sq.GetLastQuery().id); //get query id from db
                if (sq != null && res != null)
                    return new BaseResponse
                    {
                        status = (int)Enums.ServerStatus.OK,
                        query = sq.GetLastQuery().search_query,
                        searhSystem = res.Select(it => it.search_system).FirstOrDefault(),
                        data = res.Select(it => it.search_result).ToList().GetRange(offset, limit)
                    };
                else return new BaseResponse {status = (int)Enums.ServerStatus.API_UNKNOWN_ERROR};
            }
        }

        /// <summary>
        /// Realizes new search, write results into db, get back data to client
        /// </summary>
        /// <param name="query">Text of query</param>
        /// <returns>Data model - BaseResponse. Contain search results. </returns>
        public BaseResponse QueryToSearchSystem(string query)
        {
            var searchSystemList = File.ReadAllText("SearchSystem.json");
            var ssl = JsonConvert.DeserializeObject<SearchSystems>(searchSystemList).SearchSysytem;
            var searchData = GetSearchResultPage(ssl, query);
            var responseList = new List<string>();

            //checking  not empty response
            //sometimes search server can block that system, because that is machine searching
            //if search server blocked, system try to use another
            while (responseList.Count == 0)
            {
                responseList = HtmlHandler.GetHref(searchData.result, searchData.systemName).Distinct().ToList();
                if (responseList.Count == 0)
                {
                    ssl = CleanSearchSystemList(ssl, searchData.systemName);
                    searchData = GetSearchResultPage(ssl, query);
                }
            }

            if (searchData.status == (int) Enums.ServerStatus.OK)
            {
                var searchResult = new BaseResponse
                {
                    status = searchData.status,
                    query = query,
                    data = responseList.GetRange(offset, limit),
                    searhSystem = searchData.systemName
                };
                SaveSearchResultToDb(searchResult);
                return searchResult;
            }
            else return new BaseResponse {status = searchData.status};
        }

        /// <summary>
        /// saving data to db
        /// </summary>
        /// <param name="searchresults"></param>
        private void SaveSearchResultToDb(BaseResponse searchresults)
        {
            using (var db = new ApplicationContext())
            {
                if (searchresults.query.Length == 0 ) throw new Exception("Searh results is empty");
                var sq = repManager.Get<SearchQueryRepository>(db);
                var res = repManager.Get<SearchResultRepository>(db);
                var queryId = sq.Add(new search_queries
                {
                    id = Guid.NewGuid(),
                    search_query = searchresults.query
                    
                });
                res.AddResult(queryId, (List<string>)searchresults.data, searchresults.searhSystem);
            }
        }

        /// <summary>
        /// returns first completed search
        /// </summary>
        /// <param name="ssl">list of search system</param>
        /// <param name="query">text of query</param>
        /// <returns>Data model - ServerResponse struct, contain name of search system and Html code</returns>
        private ServerResponse GetSearchResultPage(List<SearchSystem> ssl, string query)
        {
            if (ssl == null && ssl.Count == 0) throw new Exception("List of search systems is empty");
            if (query == string.Empty) throw new Exception("Query is empty");
            var taskConteiner = new Task<string>[ssl.Count];
            try
            {
                for (int i = 0; i < ssl.Count; i++)
                {
                    var ind = i;
                    taskConteiner[i] = Task.Run(() => HttpHandler.GetMethod(query, ssl[ind].Uri));
                }
                var index = Task.WaitAny(taskConteiner); // id of the first ended task

                return new ServerResponse
                {
                    status = (int)Enums.ServerStatus.OK,
                    systemName = ssl[index].SystemName,
                    result = taskConteiner[index].Result
                };
            }
            catch (Exception e)
            {
                return new ServerResponse
                {
                    status = (int)Enums.ServerStatus.COMMON_HTTP_SERVICE_NOT_AVAILABLE
                };
            }
            
        }
        /// <summary>
        /// Remove blocked search server from servers list
        /// </summary>
        /// <param name="ssl"></param>
        /// <param name="filter"></param>
        /// <returns>Data model - List of SearchSystem, contain name of search system and uri for search</returns>
        private List<SearchSystem> CleanSearchSystemList(List<SearchSystem> ssl, string filter) => ssl.Where(it => it.SystemName.Equals(filter) == false).ToList();
        
        /// <summary>
        /// local data
        /// </summary>
        private struct ServerResponse
        {
            public int status { get; set; }
            public string systemName { get; set; }
            public string result { get; set; }
        }
    }
}